package tk.nascar.controllers.openidconnect.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.codec.binary.Base64;

import java.util.*;

public abstract class OpenIdConnectUtilsImpl implements OpenIdConnectUtils {

    private static final Base64 decoder = new Base64(true);
    private static final JsonParser jsonParser = new JsonParser();

    @Override
    public JsonObject authenticateByOAuthCode(
            String oAuthCode,
            String oAuthAuthorizationUrl,
            String oAuthCallbackUrl,
            String clientId,
            String clientSecret,
            String[] claims
    ) throws OpenIdConnectException {
        System.out.println("Start. Code parameter: " + oAuthCode);

        String authorizationResponse = performAuthenticationRequest(
                oAuthCode, oAuthAuthorizationUrl, oAuthCallbackUrl, clientId, clientSecret
        );

        System.out.println("Authorization response: " + authorizationResponse);

        JsonObject responseJsonObject = parseAuthorizationResponse(authorizationResponse);
        JsonObject idTokenJsonObject = responseJsonObject.get("id_token").getAsJsonObject();

        if (!isJwtValid(idTokenJsonObject)) {
            throw new OpenIdConnectException("JWT is not valid");
        }

        JsonObject result = new JsonObject();
        result.addProperty("access_token", responseJsonObject.get("access_token").getAsString());
        result.addProperty("expires_in", responseJsonObject.get("expires_in").getAsString());
        result.addProperty("token_type", responseJsonObject.get("token_type").getAsString());

        if (responseJsonObject.get("refresh_token") != null) {
            result.addProperty("refresh_token", responseJsonObject.get("refresh_token").getAsString());
        }

        JsonObject payloadJsonObject = idTokenJsonObject.get("payload").getAsJsonObject();
        for (String claim : claims) {
            result.addProperty(claim, payloadJsonObject.get(claim).getAsString());
        }

        return result;
    }

    protected String performAuthenticationRequest(
            String oAuthCode,
            String oAuthAuthorizationUrl,
            String oAuthCallbackUrl,
            String clientId,
            String clientSecret
    ) throws OpenIdConnectException {
        try {
            return Unirest.post(oAuthAuthorizationUrl)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .field("code", oAuthCode)
                    .field("client_id", clientId)
                    .field("client_secret", clientSecret)
                    .field("redirect_uri", oAuthCallbackUrl)
                    .field("grant_type", "authorization_code")
                    .asString()
                    .getBody();
        } catch (UnirestException e) {
            throw new OpenIdConnectException("There is a problem wile executing Authentication request.", e);
        }
    }

    protected JsonObject parseAuthorizationResponse(String responseBody) throws OpenIdConnectException {
        JsonElement jsonResponse = jsonParser.parse(responseBody);

        System.out.println("Parsed as JsonElement json from google: " + jsonResponse);

        if (jsonResponse == null) {
            throw new OpenIdConnectException("Bad json returned from google cannot be parsed");
        }

        if (jsonResponse.getAsJsonObject() == null) {
            throw new OpenIdConnectException("Bad json transformation to JsonObject");
        }

        if (jsonResponse.getAsJsonObject().get("access_token") == null) {
            throw new OpenIdConnectException("No access_token found in received json");
        }

        if (jsonResponse.getAsJsonObject().get("id_token") == null) {
            throw new OpenIdConnectException("No id_token found in received json");
        }

        String jwt = jsonResponse.getAsJsonObject().get("id_token").getAsString();
        if (jwt == null) {
            throw new OpenIdConnectException("Bad json transformation to string");
        }

        jsonResponse.getAsJsonObject().remove("id_token");
        jsonResponse.getAsJsonObject().add("id_token", parseJwt(jwt));

        return jsonResponse.getAsJsonObject();
    }


    protected JsonObject parseJwt(String jwt) throws OpenIdConnectException {
        String[] parts = jwt.split("\\.");

        if (parts.length != 3) {
            throw new OpenIdConnectException("JWT has wrong format: " + jwt);
        }

        System.out.printf("Split result from JWT: " + Arrays.toString(parts));

        parts[0] = new String(decoder.decode(parts[0]));
        parts[1] = new String(decoder.decode(parts[1]));

        JsonObject object = new JsonObject();
        object.add("header", jsonParser.parse(parts[0]).getAsJsonObject());
        object.add("payload", jsonParser.parse(parts[1]).getAsJsonObject());
        object.addProperty("secret", parts[2]);

        return object;
    }

    protected abstract boolean isJwtValid(JsonObject jwt);

    protected abstract boolean isClaimAvailable(String claim);

    @Override
    public String composeAuthenticationRequest(String authenticationUrl, String callbackUrl, String clientId, String[] scopes) {
        return authenticationUrl + "?" +
               "client_id=" + clientId + "&" +
               "response_type=code" + "&" +
               "scope="  + String.join(" ", scopes) + "&" +
               "redirect_uri=" + callbackUrl;
    }

    @Override
    public JsonObject requestJsonApi(String apiUrl, String accessToken) throws OpenIdConnectException {
        String responseBody = null;
        try {
            responseBody = Unirest.get(apiUrl)
                .queryString("access_token", accessToken)
                .asString()
                .getBody();
        } catch (UnirestException e) {
            throw new OpenIdConnectException("There is some problem with accessing Json API.", e);
        }

        return jsonParser.parse(responseBody).getAsJsonObject();
    }

}
