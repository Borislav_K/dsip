package tk.nascar.controllers.openidconnect.utils;

import com.google.gson.JsonObject;

public interface OpenIdConnectUtils {

    JsonObject authenticateByOAuthCode(
            String oAuthCode,
            String oAuthAuthorizationUrl,
            String oAuthCallbackUrl,
            String clientId,
            String clientSecret,
            String[] claims
    ) throws OpenIdConnectException;

    String composeAuthenticationRequest(String authenticationUrl, String callbackUrl, String clientId, String[] scopes);

    JsonObject requestJsonApi(String apiUrl, String accessToken) throws OpenIdConnectException;

    class OpenIdConnectException extends Exception {
        public OpenIdConnectException(String message) {
            super(message);
        }

        public OpenIdConnectException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
