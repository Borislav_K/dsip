package tk.nascar.controllers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tk.nascar.Constants;
import tk.nascar.controllers.openidconnect.utils.OpenIdConnectUtils;
import tk.nascar.controllers.openidconnect.utils.google.OpenIdConnectUtilsForGoogle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class OpenIDConnectController {

    @Autowired
    OpenIdConnectUtilsForGoogle openIdConnectUtilsForGoogle;

    public static final String OAUTH_CALLBACK_PATH = "openidconnectcallback";

    @GetMapping(value = OAUTH_CALLBACK_PATH)
    public String openidConnectCallback(
            @RequestParam("code") String code,
            Model model
    ) {
        try {
            JsonObject data = openIdConnectUtilsForGoogle.authenticateByOAuthCode(
                    code,
                    OpenIdConnectUtilsForGoogle.TOKEN_URL,
                    Constants.HEROKU_URL + "/" + OAUTH_CALLBACK_PATH,
                    OpenIdConnectUtilsForGoogle.CLIENT_ID,
                    OpenIdConnectUtilsForGoogle.CLIENT_SECRET,
                    new String[] {"email", "name"}
            );

            model.addAttribute("email", data.get("email").getAsString());
            model.addAttribute("name", data.get("name").getAsString());

            JsonObject threadsContainer = openIdConnectUtilsForGoogle.requestJsonApi(
                    "https://www.googleapis.com/gmail/v1/users/" + data.get("email").getAsString() + "/threads",
                    data.get("access_token").getAsString()
            );

            JsonElement threads = threadsContainer.get("threads");
            if (threads != null) {
                List<String> threadsList = new ArrayList<>(threads.getAsJsonArray().size());

                for (JsonElement thread: threads.getAsJsonArray()) {
                    threadsList.add(thread.getAsJsonObject().get("snippet").getAsString());
                }

                model.addAttribute("messages", threadsList);
            } else {
                System.out.printf("No messages");
                model.addAttribute("messages", Collections.EMPTY_LIST);
            }
        } catch (OpenIdConnectUtils.OpenIdConnectException e) {
            System.out.println(e.getStackTrace());
        }

        return "threads";
    }

    @GetMapping(value = "signin")
    public String signIn(Model model) {
        model.addAttribute(
            "openidconnect_url",
            openIdConnectUtilsForGoogle.composeAuthenticationRequest(
                    OpenIdConnectUtilsForGoogle.AUTHENTICATION_URL,
                    Constants.HEROKU_URL + "/" + OAUTH_CALLBACK_PATH,
                    OpenIdConnectUtilsForGoogle.CLIENT_ID,
                    new String[] {"openid", "email", "profile", "https://www.googleapis.com/auth/gmail.readonly"}
            )
        );

        return "signin";
    }
}
